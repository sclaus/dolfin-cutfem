/*
 * Patch.cpp
 *
 *  Created on: 20 Apr 2017
 *      Author: susanneclaus
 */


#include "PatchErrorControl.h"
#include "PatchMaker.h"
#include "PatchAssembler.h"
#include <dolfin/adaptivity/LocalAssembler.h>
#include <dolfin/mesh/Vertex.h>
#include <dolfin/mesh/Facet.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/la/GenericMatrix.h>
#include <dolfin/fem/fem_utils.h>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/fem/Form.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/fem/UFC.h>
#include <dolfin/log/log.h>
#include <dolfin/log/Progress.h>

#include <Eigen/Dense>

using namespace dolfin;

//build dofmap from patch to global dofs, map will give patch dof number when given the global dof number
//The FunctionSpace V is a function space over the refined fem space
void PatchErrorControl::build_global_to_patch_dofmap(const std::shared_ptr<const dolfin::FunctionSpace> V, const Patch &patch,std::set<std::size_t>& patch_dofs, std::unordered_map<std::size_t, std::size_t>& patch_dof_map)
{
	//Build dofmap from global dofs to patch dofs
	const dolfin::GenericDofMap* dofmaps = V->dofmap().get();

    // Vector to hold dof map for a cell
    dolfin::ArrayView<const dolfin::la_index> dofs;

	//collect set of all dofs in patch
	//std::set<std::size_t> patch_dofs;

	//get cells in patch
	auto child_cell_patch = patch.child_cell_patch();

	//Iterate over all cells in the patch to collect all dofs in patch
	for(auto it=child_cell_patch.begin(); it != child_cell_patch.end(); it++)
	{
	  // Get local-to-global dof maps for cell
	  dofs = dofmaps->cell_dofs(*it);

	  //std::cout << "cell_id: " << *it;
	 // std::cout << ", dofs[" << i << "]=[";
	  for(std::size_t j=0;j<dofs.size();j++)
	  {
		  //std::cout << dofs[i][j] << ",";
		  patch_dofs.insert(dofs[j]);
	  }
	  //std::cout << "]" << std::endl;
	}

//	std::cout << "Patch Dofs: [" << std::endl;
//	for(auto it=patch_dofs.begin(); it != patch_dofs.end(); it++)
//	{
//		std::cout << *it  << ", ";
//	}
//	 std::cout << "]" << std::endl;

	//Construct patch_dof_map
	//std::unordered_map<std::size_t, std::size_t> patch_dof_map;
	std::size_t cnt  = 0;

	for(auto it=patch_dofs.begin(); it != patch_dofs.end(); it++)
	{
	  patch_dof_map[*it] = cnt;
	  cnt++;
	}

//  std::cout << "Mapping: " << std::endl;
//  for(auto it=patch_dof_map.begin();it != patch_dof_map.end();it++)
//  {
//	  std::cout << it->first << "->" << it->second << ", ";
//  }
//  std::cout << std::endl;
//		  std::size_t rnd =76;
//		  std::cout << "map of 76:" << collapsed_map[rnd] << std::endl;
}

//Original method, solve on patch with hat function and store result in function u on the refined space
void PatchErrorControl::solve(const dolfin::Form& a_r,dolfin::Function & u, dolfin::Form& L_r, const PatchMaker& patch_maker)
{
	std::size_t num_patches = patch_maker.num_patches();
	auto patches = patch_maker.patches();

	for(std::size_t i =0; i<num_patches;i++)
	{
		//dolfin::tic();
		std::size_t vertex_id = i;
		PatchAssembler patch_assembler;
		auto V = a_r.function_space(0);

		//Map from dof number in patch to global dof number
		std::unordered_map<std::size_t, std::size_t> patch_dof_map;
		//dofs in patch
		std::set<std::size_t> patch_dofs;

		PatchErrorControl::build_global_to_patch_dofmap(V, patches[vertex_id], patch_dofs, patch_dof_map);

		//Allocate memory for patch matrices and vectors
		std::size_t num_dofs = patch_dof_map.size();
//		std::cout << "N=" << num_dofs << std::endl;

		Eigen::MatrixXd A_p_0(num_dofs,num_dofs);
		Eigen::MatrixXd b_p_0(num_dofs,1);
		Eigen::VectorXd x(num_dofs);
		x.setZero();

		//Update hat function for patch
		//std::cout << "Coefficient name=" << L_r.coefficient_name(1) << std::endl;
		L_r.set_coefficient("l_P", patches[vertex_id].hat_function());
		//L_r.l_P = *patches[vertex_id].hat_function();

		patch_assembler.assemble(A_p_0, a_r, patches[vertex_id], patch_dof_map);
		patch_assembler.assemble(b_p_0, L_r, patches[vertex_id], patch_dof_map);

//		std::cout << "Matrix A_p=" << A_p_0 << std::endl;
//		std::cout << "Matrix b_p=" << b_p_0 << std::endl;
//		std::cout << "Vector x=" << x << std::endl;

		// Solve linear system and convert result
		x = A_p_0.partialPivLu().solve(b_p_0); //fullPivLu().solve(b_p_0);

//		std::cout << "x=" << x << std::endl;

		std::vector<dolfin::la_index> patch_dofs_vec(patch_dofs.begin(),patch_dofs.end());

		u.vector()->add(x.data(),num_dofs, &patch_dofs_vec[0]);
		//u_total_patch->vector()->apply("insert");
	}
	u.vector()->apply("insert");
}

//Solve for the error density, in each patch the hat function is not used in this approach
void PatchErrorControl::solve_density_per_patch(const dolfin::Form& a_dual_r, dolfin::Function & u, dolfin::Form& L_dual_r, dolfin::Form& L_r, const PatchMaker& patch_maker)
{
	auto V_fine = a_dual_r.function_space(0);
	auto V_coarse = u.function_space();
	auto mesh = V_coarse->mesh();
	const std::vector<dolfin::la_index> vertex_to_dof_map = dolfin::vertex_to_dof_map(*V_coarse);
	std::size_t num_patches = patch_maker.num_patches();
	auto patches = patch_maker.patches();

	//std::size_t i = 6;
	for(std::size_t i =0; i<num_patches;i++)
	{
		std::size_t vertex_id = i;
		PatchAssembler patch_assembler;

		//Map from dof number in patch to global dof number
		std::unordered_map<std::size_t, std::size_t> patch_dof_map;
		//dofs in patch
		std::set<std::size_t> patch_dofs;

		PatchErrorControl::build_global_to_patch_dofmap(V_fine, patches[vertex_id], patch_dofs, patch_dof_map);

		//////////////////////////
		// Solve primal error	//
		//////////////////////////

		//Allocate memory for patch matrices and vectors
		std::size_t num_dofs = patch_dof_map.size();

		Eigen::MatrixXd b_p_0(num_dofs,1);
		patch_assembler.assemble(b_p_0, L_r, patches[vertex_id], patch_dof_map);

		//////////////////////////
		// Solve dual error	//
		//////////////////////////
		Eigen::MatrixXd A_dual_p_0(num_dofs,num_dofs);
		Eigen::MatrixXd b_dual_p_0(num_dofs,1);
		Eigen::VectorXd e_z_p(num_dofs);
		e_z_p.setZero();

		patch_assembler.assemble(A_dual_p_0, a_dual_r, patches[vertex_id], patch_dof_map);
		patch_assembler.assemble(b_dual_p_0, L_dual_r, patches[vertex_id], patch_dof_map);

		// Solve linear system and convert result
		e_z_p = A_dual_p_0.partialPivLu().solve(b_dual_p_0); //fullPivLu().solve(b_p_0);

		//get the volume of the patch
		double volume = patches[vertex_id].volume(mesh);

		auto rho_mat = b_p_0.transpose()*e_z_p;
		double rho = rho_mat(0,0);
		rho = rho/volume;

		u.vector()->setitem(vertex_to_dof_map[vertex_id], rho);
	}
	u.vector()->apply("insert");
}

//In this solver a_dual_r and L_dual_r are solved on each patch without the hat function
//The dual weighted residual in each is patch is computed with the hat function
// Save the result of patch residual in vertex center of patch
void PatchErrorControl::solve_per_patch(const dolfin::Form& a_dual_r, dolfin::Function & R_ez, dolfin::Form& L_dual_r, dolfin::Form& L_r, const PatchMaker& patch_maker)
{
	auto V = a_dual_r.function_space(0);
	const std::vector<dolfin::la_index> vertex_to_dof_map = dolfin::vertex_to_dof_map(*V);
	std::size_t num_patches = patch_maker.num_patches();
	auto patches = patch_maker.patches();

	for(std::size_t i =0; i<num_patches;i++)
	{
		//dolfin::tic();
		std::size_t vertex_id = i;
		PatchAssembler patch_assembler;

		//Map from dof number in patch to global dof number
		std::unordered_map<std::size_t, std::size_t> patch_dof_map;
		//dofs in patch
		std::set<std::size_t> patch_dofs;

		PatchErrorControl::build_global_to_patch_dofmap(V, patches[vertex_id], patch_dofs, patch_dof_map);

		//Allocate memory for patch matrices and vectors
		std::size_t num_dofs = patch_dof_map.size();

		//Solve dual problem on patch
		Eigen::MatrixXd A_p_0(num_dofs,num_dofs);
		Eigen::MatrixXd b_p_0(num_dofs,1);
		Eigen::VectorXd e_z_p(num_dofs);
		e_z_p.setZero();

		patch_assembler.assemble(A_p_0, a_dual_r, patches[vertex_id], patch_dof_map);
		patch_assembler.assemble(b_p_0, L_dual_r, patches[vertex_id], patch_dof_map);

		// Solve linear system and convert result
		e_z_p = A_p_0.partialPivLu().solve(b_p_0); //fullPivLu().solve(b_p_0);

		//Compute dual weighted residual with e_z_p and save result in center vertex
		L_r.set_coefficient("l_P",patches[vertex_id].hat_function());

		Eigen::MatrixXd b_r_p(num_dofs,1);
		patch_assembler.assemble(b_r_p, L_r, patches[vertex_id], patch_dof_map);

		auto R_ez_p_mat = b_r_p.transpose()*e_z_p;
		double R_ez_p = R_ez_p_mat(0,0);

		R_ez.vector()->setitem(vertex_to_dof_map[vertex_id], R_ez_p);
	}
}


void PatchErrorControl::compute_indicators(dolfin::MeshFunction<double>& indicators,
                                       dolfin::Function& error_density)
{
	//Take absolute value of error density
	error_density.vector()->abs();

	const dolfin::Mesh& mesh= *indicators.mesh();
	auto V = error_density.function_space();
	const dolfin::GenericDofMap& dofmap(*V->dofmap());
	const dolfin::FiniteElement& element = *V->element();

	// Initialize local arrays
	std::vector<double> cell_coefficients(dofmap.max_element_dofs());

	// Iterate over mesh and interpolate on each cell
	ufc::cell ufc_cell;
	std::vector<double> coordinate_dofs;
	for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
	{
		std::size_t cell_index = cell->index();
		// Update to current cell
		cell->get_coordinate_dofs(coordinate_dofs);
		cell->get_cell_data(ufc_cell);

		// Restrict function to cell
		error_density.restrict(cell_coefficients.data(), element, *cell,
				   coordinate_dofs.data(), ufc_cell);

		//Average over the cell
		std::size_t num_dofs = dofmap.num_element_dofs(cell_index);
		double average_value = 0.0;

		for(std::size_t i =0;i<num_dofs;i++)
		{
			average_value +=cell_coefficients[i];
		}

		double volume = cell->volume();

		average_value = average_value/num_dofs;
		average_value = average_value*volume;
		indicators[cell->index()] = average_value;
	}
}

//eta is a DG0 function, convert eta into error indicator
void PatchErrorControl::compute_indicators_DG0(dolfin::MeshFunction<double>& indicators,
                                       dolfin::Function& eta)
{
	eta.vector()->abs();

	// Convert vector of eta to indicator mesh function
	const dolfin::GenericDofMap& dofmap(*eta.function_space()->dofmap());
	const dolfin::Mesh& mesh= *indicators.mesh();

	// Convert DG_0 vector to mesh function over cells
	for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
	{
		const dolfin::ArrayView<const dolfin::la_index> dofs = dofmap.cell_dofs(cell->index());
		dolfin_assert(dofs.size() == 1);
		indicators[cell->index()] = eta.vector()->getitem(dofs[0]);
	}
}

//REMARK: the dofs associated with vertex locations can be identified in the generated header under
// the function tabulate_dof_coordinates, every dof with    dof_coordinates[0] = coordinate_dofs[0] is  a vertex
// dof coordinates are listed as i*gdim+j, i.e. in 2d coordinate_dofs[0],coordinate_dofs[1] is coordinate of vertex 0
void PatchErrorControl::get_restricted_vertex_dofs_DG_space(const std::shared_ptr<dolfin::FunctionSpace> DG2, std::vector<dolfin::la_index> &vertex_dofs)
{
//Test to deactivate degrees of freedom in vertices
	 // Build sparsity pattern for vertex/point integrals
	auto mesh = DG2->mesh();
	const std::size_t D = mesh->topology().dim();

	mesh->init(0);
	mesh->init(0, D);

	//For DG element of polynomial degree 2, the first listed dofs are the one in the vertices
	//TODO: for vector valued or mixed spaces value needs to be computed differently, not sure how yet

	//Build dofmap from global dofs to patch dofs
	const dolfin::GenericDofMap* dofmaps = DG2->dofmap().get();
	auto num_dofs = dofmaps->num_element_dofs(0); // V_tm->element()->space_dimension();//  same ?
	std::size_t value_size = DG2->element()->value_dimension(0);
	std::size_t offset = num_dofs/value_size;
	std::size_t num_cells = mesh->num_cells();

	dolfin::Cell cell(*mesh,0);
	std::size_t num_vertices = cell.num_vertices();

	//for each cell we have num_vertices and in each vertex we have the number of dofs of value_size
	std::size_t num_vertex_dofs = num_cells*num_vertices*value_size;

	// Resize local dof map vector
	vertex_dofs.resize(num_vertex_dofs);

	std::size_t index_cnt = 0;

	for (dolfin::CellIterator cell(*mesh); !cell.end(); ++cell)
	{
		auto dofs = dofmaps->cell_dofs(cell->index());
		std::size_t loop_offset = 0;
		//std::cout << "cell=" << cell->index() << ", dof_size=" << dofs.size() << std::endl;

		for (std::size_t i = 0; i < value_size; ++i)
		{
			for(std::size_t j =loop_offset; j < num_vertices+loop_offset; ++j)
			{
				vertex_dofs[index_cnt]=dofs[j];
				index_cnt++;
				//std::cout << "index_cnt" << index_cnt << std::endl;
			}
			loop_offset +=offset;
		}
//		std::cout << "dofs= ";
//		for (std::size_t j = 0; j < dofs.size(); ++j)
//		{
//			std::cout  << dofs[j] << ",";
//		}
//		std::cout << "------------------------------------------" << std::endl;
	}
}

void PatchErrorControl::apply_restriction(dolfin::GenericMatrix* A,
        dolfin::GenericVector* b, const std::vector<dolfin::la_index> &vertex_dofs, const std::shared_ptr<dolfin::FunctionSpace> DG2)
{
	  const std::size_t size = vertex_dofs.size();
	  std::vector<double> values(size,0.0);
	  bool use_ident_l = false;

	  // Modify RHS vector (b[i] = value) and apply changes
	  if (b)
	  {
	    b->set_local(values.data(), size, vertex_dofs.data());
	    b->apply("insert");
	  }

	  // Modify linear system (A_ii = 1) and apply changes
	  if (A)
	  {
	    const bool use_ident = use_ident_l;
	    if (use_ident)
	      A->ident_local(size, vertex_dofs.data());
	    else
	    {
	      A->zero_local(size, vertex_dofs.data());

	      const std::size_t offset
	        = DG2->dofmap()->ownership_range().first;
	      for (std::size_t i = 0; i < size; i++)
	      {
	        std::pair<std::size_t, std::size_t> ij(offset + vertex_dofs[i],
	                                               offset + vertex_dofs[i]);
	        A->setitem(ij, 1.0);
	      }
	    }

	    // Apply changes
	    A->apply("insert");
	  }
}

void PatchErrorControl::restrict_vertices_DG2(dolfin::GenericMatrix& A,
		dolfin::GenericVector& b, const std::shared_ptr<dolfin::FunctionSpace> DG2)
{
	std::vector<dolfin::la_index> vertex_dofs;
	PatchErrorControl::get_restricted_vertex_dofs_DG_space(DG2, vertex_dofs);
	PatchErrorControl::apply_restriction(&A,&b,vertex_dofs, DG2);
}

void PatchErrorControl::solve_local(const dolfin::Form& a_R_T, const dolfin::Form& L_R_T, dolfin::Function& R_T)
{
  // Extract common space, mesh and dofmap
  const dolfin::FunctionSpace& V = *R_T.function_space();
  dolfin_assert(V.mesh());
  const Mesh& mesh(*V.mesh());
  dolfin_assert(V.dofmap());
  const dolfin::GenericDofMap& dofmap = *V.dofmap();

  // Create data structures for local assembly data
  dolfin::UFC ufc_lhs(a_R_T);
  dolfin::UFC ufc_rhs(L_R_T);

  // Define matrices for cell-residual problems
  dolfin_assert(V.element());
  const std::size_t N = V.element()->space_dimension();
  Eigen::MatrixXd A(N, N), b(N, 1);
  Eigen::VectorXd x(N);

  // Extract cell_domains etc from right-hand side form
//  const MeshFunction<std::size_t>*
//    cell_domains = L_R_T.cell_domains().get();
//  const MeshFunction<std::size_t>*
//    exterior_facet_domains = L_R_T.exterior_facet_domains().get();
//  const MeshFunction<std::size_t>*
//    interior_facet_domains = L_R_T.interior_facet_domains().get();

  // Assemble and solve local linear systems
  ufc::cell ufc_cell;
  std::vector<double> coordinate_dofs;
  for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
  {
    // Get cell vertices
    cell->get_coordinate_dofs(coordinate_dofs);

    // Assemble local linear system
    dolfin::LocalAssembler::assemble(A, ufc_lhs, coordinate_dofs,
                             ufc_cell, *cell, a_R_T.cell_domains().get(),
							 a_R_T.exterior_facet_domains().get(), a_R_T.interior_facet_domains().get());
    dolfin::LocalAssembler::assemble(b, ufc_rhs, coordinate_dofs, ufc_cell,
                             *cell, L_R_T.cell_domains().get(),
							 L_R_T.exterior_facet_domains().get(), L_R_T.interior_facet_domains().get());

    //Deactivate dofs of vertices for DG 2 scalar finite element space
    for(std::size_t i=0;i<3;i++)
    {
    	A.row(i).setZero();
    	A(i,i) = 1.0;
    	b(i,0) = 0.0;
    }

//    std::cout << "A=" << A << std::endl;
//    std::cout << "b=" << b << std::endl;

    // Solve linear system and convert result
    x = A.partialPivLu().solve(b);

    // Get local-to-global dof map for cell
    const dolfin::ArrayView<const dolfin::la_index> dofs = dofmap.cell_dofs(cell->index());

    // Plug local solution into global vector
    dolfin_assert(R_T.vector());
    R_T.vector()->set(x.data(), N, &dofs[0]);
  }
}

//Assemble Functional of dual weighted residual in each element
void PatchErrorControl::compute_R_ez_T(const dolfin::Form& a, dolfin::Function& R_T)
{
	std::vector<double> tensor(1);
	dolfin_assert(a_T.rank() == 0);
	const dolfin::FunctionSpace& V = *R_T.function_space();
	dolfin_assert(V.mesh());
	const Mesh& mesh(*V.mesh());
	const dolfin::GenericDofMap& dofmap_R = *V.dofmap();

	Eigen::MatrixXd A(1,1);
	A.setZero();
	dolfin::UFC ufc(a);
	ufc::cell ufc_cell;
	std::vector<double> coordinate_dofs;

	// Extract cell_domains etc from the form
	const dolfin::MeshFunction<std::size_t>* cell_domains =
	a.cell_domains().get();
	const dolfin::MeshFunction<std::size_t>* exterior_facet_domains =
	a.exterior_facet_domains().get();
	const dolfin::MeshFunction<std::size_t>* interior_facet_domains =
	a.interior_facet_domains().get();


	for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
	{
		cell->get_coordinate_dofs(coordinate_dofs);
		dolfin::LocalAssembler::assemble(A, ufc, coordinate_dofs,
							   ufc_cell, *cell, cell_domains,
							   exterior_facet_domains, interior_facet_domains);

		//std::cout << "A=" << A << std::endl;

		//Get dof of dofmap for cell DG 0 field
		auto dof = dofmap_R.cell_dofs(cell->index());

		R_T.vector()->setitem(dof[0], A(0,0));
	}
}

