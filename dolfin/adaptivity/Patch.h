/*
 * Patch.h
 *
 *  Created on: 20 Apr 2017
 *      Author: susanneclaus
 */

#ifndef __PATCH_H_
#define __PATCH_H_

#include <dolfin/mesh/MeshFunction.h>

namespace dolfin
{
	class Mesh;
	template<typename T> class MeshFunction;
	class GenericFunction;
	class FunctionSpace;
	class Point;
	class FiniteElement;

	class Patch
	{
		public:
			Patch();

			void create_vertex_patch(const std::shared_ptr<dolfin::Mesh> coarse_mesh, const std::size_t& vertex_id);
			void build(const std::shared_ptr<dolfin::Mesh> refined_mesh,const std::vector<std::set<std::size_t>>& reverse_cell_map);
			void build_child_patch(const std::vector<std::set<std::size_t>>& reverse_cell_map);
			void mark_child_patch_cells(const std::shared_ptr<dolfin::Mesh> refined_mesh);
			void mark_child_patch_facets(const std::shared_ptr<dolfin::Mesh> refined_mesh);
			void mark_child_patch_facets_2(const std::shared_ptr<dolfin::Mesh> refined_mesh);
			void create_hat_function(std::shared_ptr<dolfin::FunctionSpace> V, const std::vector<dolfin::la_index>& vertex_to_dof_map, const std::size_t& vertex_id);


			double volume(const std::shared_ptr<const dolfin::Mesh> coarse_mesh);


			const std::shared_ptr<dolfin::MeshFunction<size_t>> cell_marker() const
			{
				return _cell_marker;
			}

			const std::shared_ptr<dolfin::MeshFunction<size_t>> child_facet_marker() const
			{
				return _child_facet_marker;
			}

			const std::shared_ptr<dolfin::Function> hat_function() const
			{
				return _hat_function;
			}

			const std::set<size_t>& child_cell_patch() const
			{
				return _child_cell_patch;
			}

			const std::set<std::size_t>& exterior_facets_child_patch() const
			{
				return _exterior_facets_child_patch;
			}

			const std::set<std::size_t>& exterior_interior_facets_child_patch() const
			{
				return _exterior_interior_facets_child_patch;
			}

			const std::set<std::size_t>& interior_facets_child_patch() const
			{
				return _interior_facets_child_patch;
			}

		private:
			//vertex id
			std::size_t _vertex_id;
			//Indices of cells in coarse mesh/parent mesh that form the patch
			std::vector<size_t> _parent_cell_patch;

			//Number of indices in child patch
			std::set<size_t> _child_cell_patch;

			//Save the index of exterior facets of parent patch and construct from this the exterior facets indices
		  //for the child mesh
		  std::vector<std::size_t> _exterior_interior_facets_parent_patch;
		  // facets that are at the boundary of the patch but interior to the parent mesh
		  std::set<std::size_t> _exterior_interior_facets_child_patch;
		  //facets that are part of the boundary of the mesh
		  std::set<std::size_t> _exterior_facets_child_patch;
		  //interior facets of child patch
		  std::set<std::size_t> _interior_facets_child_patch;

		  //Marker
		  //Mark child_cell_patch with 0
		  std::shared_ptr<dolfin::MeshFunction<size_t>> _cell_marker;
		  std::shared_ptr<dolfin::MeshFunction<size_t>> _child_facet_marker;

		  //Keep hat function for each patch
		  std::shared_ptr<dolfin::Function> _hat_function;
	};
}

#endif /* CUTFEM_ADAPTIVITY_PATCH_H_ */
