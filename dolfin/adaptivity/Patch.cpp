/*
 * Patch.cpp
 *
 *  Created on: 20 Apr 2017
 *      Author: susanneclaus
 */


#include "Patch.h"
#include <dolfin/mesh/Vertex.h>
#include <dolfin/mesh/Facet.h>
#include <dolfin/mesh/Cell.h>
#include <dolfin/function/Function.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/fem/fem_utils.h>

using namespace dolfin;

Patch::Patch()
{

}

void Patch::create_vertex_patch(const std::shared_ptr<dolfin::Mesh> coarse_mesh, const std::size_t& vertex_id)
{
	_vertex_id = vertex_id;
	//initialise vertex to cell connectivity for coarse mesh
  std::size_t gdim=coarse_mesh->geometry().dim();
  coarse_mesh->init(0, gdim);

  //Define patch based on coarse mesh vertex
  //std::size_t center_vertex_id = 10;
  dolfin::Vertex center_vertex(*coarse_mesh,vertex_id);

  // get number of cells connected to vertex
  std::size_t num_cells = center_vertex.num_entities(gdim);
  auto connected_cells = center_vertex.entities(gdim);

  //std::set<size_t> parent_cell_patch;
  for(std::size_t j=0;j<num_cells;j++)
  {
	  std::size_t cell_id = connected_cells[j];
	  //std::cout << "cell_id= " << cell_id << std::endl;
	  _parent_cell_patch.push_back(cell_id);
  }
}

void Patch::build(const std::shared_ptr<dolfin::Mesh> refined_mesh,const std::vector<std::set<std::size_t>>& reverse_cell_map)
{
	build_child_patch(reverse_cell_map);

	//mark_child_patch_cells(refined_mesh);

	mark_child_patch_facets_2(refined_mesh);
}

void Patch::build_child_patch(const std::vector<std::set<std::size_t>>& reverse_cell_map)
{
	//Save child patch indices  by iterating over parent cells in patch
	// and then getting all the cells in parent cells through the reversed map
	std::size_t cnt = 0;
	for ( auto it = _parent_cell_patch.begin(); it != _parent_cell_patch.end(); it++ )
	{
		std::set<size_t> child_cell_set = reverse_cell_map[*it];
		for ( auto it_2 = child_cell_set.begin(); it_2 != child_cell_set.end(); it_2++ )
		{
			// std::cout << "child_cell_patch =" << *it_2 << std::endl;
			 _child_cell_patch.insert(*it_2);
			// cnt++;
		}
	}

//	_child_cell_patch = std::vector<size_t>(cnt);
//	cnt = 0;
//
//	for ( auto it = _parent_cell_patch.begin(); it != _parent_cell_patch.end(); it++ )
//	{
//		std::set<size_t> child_cell_set = reverse_cell_map[*it];
//		for ( auto it_2 = child_cell_set.begin(); it_2 != child_cell_set.end(); it_2++ )
//		{
//			// std::cout << "child_cell_patch =" << *it_2 << std::endl;
//			 _child_cell_patch[cnt] = *it_2;
//			// cnt++;
//		}
//	}
}

void Patch::mark_child_patch_cells(const std::shared_ptr<dolfin::Mesh> refined_mesh)
{
	 //Mark child_cell_patch with cell marker
	  _cell_marker = std::make_shared<dolfin::CellFunction<size_t>>(*refined_mesh,1);
	  auto & cell_marker = *_cell_marker;

	  for ( auto it =_child_cell_patch.begin(); it != _child_cell_patch.end(); it++ )
	  {
		 std::size_t child_cell_index = *it;
		 cell_marker[*it] = 0;
	  }

//	  dolfin::File file_patch_marker("./results/patches.pvd");
//	  file_patch_marker << *_cell_marker;
}

void Patch::mark_child_patch_facets(const std::shared_ptr<dolfin::Mesh> refined_mesh)
{
	//cell marker should have been filled before
	auto & cell_marker = *_cell_marker;
	std::size_t gdim = refined_mesh->geometry().dim();

	  //Mark facets of child patch
	  _child_facet_marker = std::make_shared<dolfin::FacetFunction<size_t>>(refined_mesh,1);
	  auto & child_facet_marker = *_child_facet_marker;

	  std::set<std::size_t> facets_done;

	  refined_mesh->init(gdim - 1, gdim);

	  //Find all exterior_interior and exterior facets using these markers
	  for ( auto it =_child_cell_patch.begin(); it != _child_cell_patch.end(); it++ )
	  {
		  dolfin::Cell cell(*refined_mesh,*it);
		  for (dolfin::FacetIterator facet(cell); !facet.end(); ++facet)
		  {
			  std::size_t num_f_cells = facet->num_entities(gdim);
			  auto connected_cells = facet->entities(gdim);
			  if(num_f_cells==2)
			  {
				  auto marker0 =  cell_marker[connected_cells[0]];
				  auto marker1 =  cell_marker[connected_cells[1]];

				  //std::cout << "markers=" << marker0 << "," << marker1 << std::endl;

				  if(marker0 == 0 && marker1 == 0)
				  {
					  _interior_facets_child_patch.insert(facet->index());
					  child_facet_marker[facet->index()]=2;
					  //std::cout << "interior_facet" << facet->index() << std::endl;
				  }

				  if((marker0 == 1 && marker1 == 0)||(marker0 == 0 && marker1 == 1))
				  {
					  _exterior_interior_facets_child_patch.insert(facet->index());
					  child_facet_marker[facet->index()]=0;
				  }
			  }
			  else
			  {
				  _exterior_facets_child_patch.insert(facet->index());
				  child_facet_marker[facet->index()]=0;
			  }
		  }
	  }

//	  dolfin::File file_patch_facet_marker("./results/patch_facets.pvd");
//	  file_patch_facet_marker << *_child_facet_marker;
}

void Patch::mark_child_patch_facets_2(const std::shared_ptr<dolfin::Mesh> refined_mesh)
{
	//cell marker should have been filled before
//	auto & cell_marker = *_cell_marker;
	std::size_t gdim = refined_mesh->geometry().dim();

	  //Mark facets of child patch
//	  _child_facet_marker = std::make_shared<dolfin::FacetFunction<size_t>>(refined_mesh,1);
//	  auto & child_facet_marker = *_child_facet_marker;
//
//	  std::set<std::size_t> facets_done;

	  refined_mesh->init(gdim - 1, gdim);

	  //Find all exterior_interior and exterior facets using these markers
	  for ( auto it =_child_cell_patch.begin(); it != _child_cell_patch.end(); it++ )
	  {
		  dolfin::Cell cell(*refined_mesh,*it);
		  for (dolfin::FacetIterator facet(cell); !facet.end(); ++facet)
		  {
			  //Left or right adjacent facet will be cell
			  std::size_t num_f_cells = facet->num_entities(gdim);
			  auto connected_cells = facet->entities(gdim);

			  if(num_f_cells==2)
			  {
				  //cell 0 is current cell in patch
				  if(*it==connected_cells[0])
				  {
					  //if other cell is also in child cell patch its an interior facet
					  if(_child_cell_patch.find(connected_cells[1])!=_child_cell_patch.end())
					  {
						  _interior_facets_child_patch.insert(facet->index());
					  }
					  //other cell is not in patch
					  else
					  {
						  _exterior_interior_facets_child_patch.insert(facet->index());
					  }
				  }
				  //otherwise other connected cell is current cell
				  else
				  {
					  //if other cell is also in child cell patch its an interior facet
					  if(_child_cell_patch.find(connected_cells[0])!=_child_cell_patch.end())
					  {
						  _interior_facets_child_patch.insert(facet->index());
					  }
					  //other cell is not in patch
					  else
					  {
						  _exterior_interior_facets_child_patch.insert(facet->index());
					  }
				  }

			  }
			  else
			  {
				  _exterior_facets_child_patch.insert(facet->index());
			  }
		  }
	  }

//	  dolfin::File file_patch_facet_marker("./results/patch_facets.pvd");
//	  file_patch_facet_marker << *_child_facet_marker;
}


void Patch::create_hat_function(std::shared_ptr<dolfin::FunctionSpace> V, const std::vector<dolfin::la_index>& vertex_to_dof_map, const std::size_t& vertex_id)
{
	_hat_function = std::make_shared<dolfin::Function>(V);
	 double value = 1.0;
	 //Set hat function to 1.0 in patch centre given by vertex id and keep 0 everywhere else
	 _hat_function->set_allow_extrapolation(true);
	 _hat_function->vector()->setitem(vertex_to_dof_map[vertex_id], 1.0);
}

double Patch::volume(const std::shared_ptr<const dolfin::Mesh> coarse_mesh)
{
	double volume = 0;
	for ( auto it = _parent_cell_patch.begin(); it != _parent_cell_patch.end(); it++ )
	{
		dolfin::Cell cell(*coarse_mesh,*it);
		volume += cell.volume();
	}

	return volume;
}
