/*
 * Patch.h
 *
 *  Created on: 20 Apr 2017
 *      Author: susanneclaus
 */

#ifndef __PATCHASSEMBLER_H_
#define __PATCHASSEMBLER_H_

#include <dolfin/mesh/MeshFunction.h>
#include <dolfin/adaptivity/Patch.h>
#include <unordered_map>
#include <Eigen/Dense>

namespace dolfin
{
  class Mesh;
  template<typename T> class MeshFunction;
  class GenericFunction;
  class FunctionSpace;
  class Point;
  class FiniteElement;
  class Form;
  class UFC;
  class Cell;

	class PatchAssembler
	{
		public:
			PatchAssembler()
			{};

			void assemble(Eigen::MatrixXd& A, const dolfin::Form& a
					, const dolfin::Patch& patch
					, const std::unordered_map<std::size_t, std::size_t>& patch_dof_map);
			void assemble_cells(
			  Eigen::MatrixXd& A,
			  const dolfin::Form& a,
			  dolfin::UFC& ufc,
			  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> domains,
			  const std::unordered_map<std::size_t, std::size_t>& patch_dof_map,
			  const std::set<size_t>& child_cell_patch);

			void assemble_exterior_facets(
					  Eigen::MatrixXd& A,
					  const dolfin::Form& a,
					  dolfin::UFC& ufc,
					  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> domains,
					  const std::unordered_map<std::size_t, std::size_t>& patch_dof_map,
					  const std::set<std::size_t>& exterior_facets_child_patch);

			void assemble_interior_exterior_facets(
					  Eigen::MatrixXd& A,
					  const dolfin::Form& a,
					  dolfin::UFC& ufc,
					  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> domains,
					  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> cell_domains,
					  const std::unordered_map<std::size_t, std::size_t>& patch_dof_map,
					  const std::set<size_t>& child_cell_patch,
					  const std::set<std::size_t>& exterior_facets_child_patch);

			void assemble_interior_facets(
			  Eigen::MatrixXd& A,
			  const dolfin::Form& a,
			  dolfin::UFC& ufc,
			  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> domains,
			  std::shared_ptr<const dolfin::MeshFunction<std::size_t>> cell_domains,
			  const std::unordered_map<std::size_t, std::size_t>& patch_dof_map,
			  const std::set<std::size_t>& interior_facets_child_patch);

			void assemble_local(const dolfin::Form& a,
			                    const dolfin::Cell& cell,
			                    std::vector<double>& tensor);
		private:

	};
}




#endif /* CUTFEM_ADAPTIVITY_PATCH_H_ */
