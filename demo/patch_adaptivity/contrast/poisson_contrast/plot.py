import matplotlib.pyplot as plt
from numpy import *

N_array= array([   142.,    185.,    281.,    477.,    815.,   1484.,   2656.,
                4694.,   8774.,  15523.])
eta_h= array([  1.17952910e-03,   6.88176352e-04,   3.57372865e-04,
              2.19324721e-04,   1.21292534e-04,   6.98782117e-05,
              3.47011078e-05,   2.11258172e-05,   1.04119831e-05,
              6.15198371e-06])
efficiency_array= array([ 1.58729427,  1.4139068 ,  1.23403029,  1.22734015,  1.19310398,
                         1.20968856,  1.15434508,  1.16034642,  1.12235928,  1.13632259])

plt.semilogx(N_array, efficiency_array, 'b', marker='o')

#N_array= [   142,    307,    593,   1142,   2105,   3956,   7141,  13120,  23427]
#eta_h= [ 0.02068669,  0.01056601,  0.00623967,  0.00343038 , 0.00182417 , 0.00113038, 0.00060748,  0.00036185,  0.00020674]
#efficiency_array= [ 1.16009946,  1.28010766,  1.28272466,  1.47256713 , 1.45428329 , 1.67025174, 1.81236598 , 2.23347624 , 4.04546456]
#
#plt.semilogx(N_array, efficiency_array, 'r', marker='x')

n_refinements
#plt.semilogx(N_array, ones, 'b', marker='o')
plt.xlabel('N_dof')
plt.ylabel('efficiency')
#plt.ylim((0.5,1.5))
plt.savefig('efficiency_DG2.png')
plt.show()