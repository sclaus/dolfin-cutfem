"This demo program demonstrates various algorithms for mesh smoothing."

# Copyright (C) 2010, 2015 Anders Logg
#
# This file is part of DOLFIN.
#
# DOLFIN is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# DOLFIN is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with DOLFIN. If not, see <http://www.gnu.org/licenses/>.
#
# First added:  2010-03-02
# Last changed: 2010-05-11

from dolfin import *
from mshr import *
parameters["refinement_algorithm"] = "plaza"

N = 10
# Create rectangular mesh
#mesh = RectangleMesh(Point(0.0, 0.0), Point(0.5, 0.4), 10, 10)
rectangle = Rectangle(Point(-1.0, -1.0), Point(1.0, 1.0))
domain = rectangle

segments = N
gdim = 2
circle = Circle(Point(0.0,0.0),0.5,segments)

domain.set_subdomain(1,circle)

# Create mesh
mesh = generate_mesh(domain, N)

mesh.init(gdim,gdim-1)
mesh.init()

cell_markers = MeshFunction('size_t', mesh, gdim, mesh.domains())
boundaries = FacetFunction("size_t", mesh)
boundaries.set_all(0)

file_m = File("./results/mesh.pvd")
file_b = File("./results/boundaries.pvd")

# ---------------------------------------------------------------------- #
# Create facet marker for facets between particles and background domain #
# ---------------------------------------------------------------------- #
for facet in facets(mesh): # For all edges in the mesh
    num_f_cells = facet.num_entities(gdim)
    
    if(num_f_cells ==2 ): #interior edge
        cell_index_0 = int(facet.entities(gdim)[0])
        cell_index_1 = int(facet.entities(gdim)[1])
        marker0 =  cell_markers[cell_index_0]
        marker1 =  cell_markers[cell_index_1]
        
        if((marker0 == 1 and marker1 == 0)or(marker0 == 0 and marker1 == 1)):
            boundaries[facet.index()]=2


file_b << boundaries
file_m << mesh

# Define a circular hole
center = Point(0, 0)
radius = 0.5
class Hole(SubDomain):

    def inside(self, x, on_boundary):
        r = sqrt((x[0] - center[0])**2 + (x[1] - center[0])**2)
        return r < 1.5*radius # slightly larger

    def snap(self, x):
        r = sqrt((x[0] - center[0])**2 + (x[1] - center[1])**2)
        if r < 1.5*radius:
            x[0] = center[0] + (radius / r)*(x[0] - center[0])
            x[1] = center[1] + (radius / r)*(x[1] - center[1])

# Mark hole and extract submesh
hole = Hole()

#sub_domains = MeshFunction("size_t", mesh, mesh.topology().dim())
#sub_domains.set_all(0)
#hole.mark(sub_domains, 1)
#
#plot(sub_domains)
#
#mesh = SubMesh(mesh, sub_domains, 0)
#mesh.snap_boundary(hole)

# Refine and snap mesh
plot(mesh, title="Mesh 0")
num_refinements = 2
for i in range(num_refinements):

    # Mark cells for refinement
    markers = MeshFunction("bool", mesh, mesh.topology().dim())
    markers.set_all(False)
    for cell in cells(mesh):
        if cell.midpoint().distance(center) < 1.1*radius and cell.midpoint().distance(center) > 0.9*radius:
            markers[cell.index()] = True

    # Refine mesh
    mesh = refine(mesh, markers)

    mesh.init()

    cell_markers_old = cell_markers
    #cell_markers = adapt(cell_markers,mesh.leaf_node())
    #mesh.clear_child()
    
    cell_markers = MeshFunction('size_t', mesh, gdim, mesh.domains())
    
    if (mesh.data().exists("parent_cell", gdim)):
        parent = (mesh.data().array("parent_cell", gdim))
        size_p = len(parent)
        print size_p
        for k in range(0, size_p):
            parent_index = int(parent[k])
            cell_markers[k] = cell_markers_old[parent_index]

    boundaries = FacetFunction("size_t", mesh)
    boundaries.set_all(0)

    # ---------------------------------------------------------------------- #
    # Create facet marker for facets between particles and background domain #
    # ---------------------------------------------------------------------- #
    for facet in facets(mesh): # For all edges in the mesh
        num_f_cells = facet.num_entities(gdim)
        
        if(num_f_cells ==2 ): #interior edge
            cell_index_0 = int(facet.entities(gdim)[0])
            cell_index_1 = int(facet.entities(gdim)[1])
            marker0 =  cell_markers[cell_index_0]
            marker1 =  cell_markers[cell_index_1]
            
            if((marker0 == 1 and marker1 == 0)or(marker0 == 0 and marker1 == 1)):
                boundaries[facet.index()]=2


    # Snap boundary
    mesh.snap_boundary_marker(hole,boundaries,2)

    file_b << boundaries
    file_m << mesh

    # Plot mesh
    #plot(mesh, title=("Mesh %d" % (i + 1)))
    # Plot mesh
    #plot(boundaries, title=("Mesh %d" % (i + 1)))

#interactive()
